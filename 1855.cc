#include <bits/stdc++.h>

using namespace std;

string mapa[106];
int vis[106][106];
int main()
{
	int x, y;
	
	while (cin >> y >> x)
	{
		for (int i = 0 ; i < x; ++i) cin >> mapa[i];
		int flag = 0;
		int goi = 0;
		int goj = 1;
		int i = 0;
		int j = 0;
		memset(vis, 0, sizeof vis);
		
		while (1)
		{
		    switch(mapa[i][j]) {
		        case '>':
		            goi = 0;
				    goj = 1;
		            break;
                case '<':
                    goi = 0;
				    goj = -1;
                    break;
                case 'v':
                    goi = 1;
				    goj = 0;
                    break;
                case '^':
                    goi = -1;
				    goj = 0;
                    break;
                case '*':
                    cout << "*\n";
    				flag = 1;
    				return 0;
                    break;
		    }
			i += goi;
			j += goj;
			if (vis[i][j] || i >= x || j >= y) break;
			vis[i][j] = 1;
		}
		if (!flag) cout << "!\n";
	}
}